
# This file was generated by the `freeze_debianver` command in setup.py
# Using 'versioneer.py' (0.16) from
# revision-control system data, or from the parent directory name of an
# unpacked source archive. Distribution tarballs contain a pre-generated copy
# of this file.

import json
import sys

version_json = '''
{
 "dirty": false,
 "error": null,
 "full-revisionid": "811410d24215fb0539f1f0047bf0c621389b3423",
 "version": "0.4.2"
}
'''  # END VERSION_JSON

def get_versions():
    return json.loads(version_json)
